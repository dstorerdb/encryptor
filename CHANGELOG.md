- 0.1
    - Encriptador realizado y testeado

- 0.2
    - Se añadio mas seguridad al Encryptor

- 1.0
    - Implementado un nuevo algoritmo de doble encriptación apoyándose en el uso de llaves (Key)
    
- 2.0
    - Migracion a estructura maven
    
- 2.0.1
    - Se eliminan redundancias de codigo