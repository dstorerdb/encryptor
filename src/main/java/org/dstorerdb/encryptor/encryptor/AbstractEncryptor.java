/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.encryptor.encryptor;

import org.dstorerdb.encryptor.exceptions.InvalidTextException;

import static org.dstorerdb.encryptor.encryptor.Encryptor.NULL;

/**
 *
 * @author martin
 */
public abstract class AbstractEncryptor {
    protected Key key;
    protected StringBuilder sBuilder;
    protected static final String DEFAULT_KEY_STR = "powerx7";

    public AbstractEncryptor() {
        this(DEFAULT_KEY_STR);
    }

    public AbstractEncryptor(String strKey){
        key = new Key(strKey);
        sBuilder = new StringBuilder();
    }

    protected void defaultEncrypt(char[] txtChars, StringBuilder sBuilder) {
        final int charsLen = txtChars.length;

        for (int i = 0; i < charsLen; i++)
            sBuilder.append(getEncryptedChar(txtChars[i]));

        sBuilder.append(getEncryptedChar(NULL));
        final char[] keyChars = key.getChars(1, key.getKeyLenght());
        final int kcLen = keyChars.length;

        for (int i = 0; i < kcLen; i++)
            sBuilder.append(getEncryptedChar(keyChars[i]));
    }
    
    protected abstract char getEncryptedChar(char c);
    protected abstract char getDecryptedChar(char c);
    protected abstract String encrypt(String text) throws InvalidTextException;
    protected abstract String decrypt(String text) throws InvalidTextException;
}
