/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.encryptor.encryptor;

import org.dstorerdb.encryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class SysEncryptor extends AbstractEncryptor{

    private static final char NULL = 21;
    
    public SysEncryptor() {
        super();
    }

    @Override
    protected char getEncryptedChar(char c){
        return (char) (((int)c)-5);
    }
    
    @Override
    protected char getDecryptedChar(char c){
        return (char) (((int)c)+5);
    }
    
    @Override
    public synchronized String encrypt(String text) throws InvalidTextException {
        sBuilder.delete(0, sBuilder.length());
        defaultEncrypt(text.toCharArray(), sBuilder);
        return sBuilder.toString();
    }
    
    @Override
    public synchronized String decrypt(String text) throws InvalidTextException{
        if (text == null || text.isEmpty())
            throw new InvalidTextException("El texto a desencriptar es invalido");

        sBuilder.delete(0, sBuilder.length());
        final char[] txtChars = text.toCharArray();
        final int charsLen = txtChars.length;
        
        char c;
        for (int i = 0; i < charsLen && (c = getDecryptedChar(txtChars[i])) != NULL; i++)
            sBuilder.append(c);

        return sBuilder.toString();
    }
//    
//    public static void main(String[] args) throws InvalidTextException {
//        Encryptor enc = new Encryptor();
//        String text = "hola";
//        String e = enc.encrypt(text);
//        String d = enc.decrypt(e);
//        System.out.println(e);
//        System.out.println(d);
//    }
    
}
