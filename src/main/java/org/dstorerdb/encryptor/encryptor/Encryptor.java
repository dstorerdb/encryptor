/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.encryptor.encryptor;

import org.dstorerdb.encryptor.exceptions.InvalidTextException;

/**
 *
 * @author martin
 */
public class Encryptor extends AbstractEncryptor{

    private final SysEncryptor SYS_ENCRYPTOR;
    
    protected static final byte MULTIPLIER = -1;
    protected static final byte ADDER = 5;
    protected static final char NULL = 20;
    
    public Encryptor() {
        this(DEFAULT_KEY_STR);
    }
    
    public Encryptor(String strKey){
        super(strKey);
        this.SYS_ENCRYPTOR = new SysEncryptor();
    }
    
    @Override
    protected char getEncryptedChar(char c){
        return (char) (((int)c)-5);
    }
    
    @Override
    protected char getDecryptedChar(char c){
        return (char) (((int)c)+5);
    }
    
    @Override
    public synchronized String encrypt(String text) throws InvalidTextException{
        if (text == null || text.isEmpty())
            throw new InvalidTextException("El texto a encriptar es invalido");
        
        sBuilder.delete(0, sBuilder.length());
        sBuilder.append(getEncryptedChar(key.getFirstChar()));
        defaultEncrypt(text.toCharArray(), sBuilder);
        return SYS_ENCRYPTOR.encrypt(sBuilder.toString());
    }
        
    @Override
    public synchronized String decrypt(String text) throws InvalidTextException{
        if (text == null || text.isEmpty())
            throw new InvalidTextException("El texto a desencriptar es invalido");
        
        text = SYS_ENCRYPTOR.decrypt(text);

        sBuilder.delete(0, sBuilder.length());
        final char[] txtChars = text.toCharArray();
        final int charsLen = txtChars.length;
        
        char c;
        for (int i = 1; i < charsLen && (c = getDecryptedChar(txtChars[i])) != NULL; i++)
            sBuilder.append(c);

        return sBuilder.toString();
    }

    public static void main(String[] args) throws InvalidTextException {
        Encryptor enc = new Encryptor();
        String txt = "errord-Orden no valida/002";
        String e = enc.encrypt(txt);
        String d = enc.decrypt(e);
        System.out.println(txt);
        System.out.println(e);
        System.out.println(d);
        System.out.println("Are Equals: "+txt.equals(d));
    }
    
}
