/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.encryptor.encryptor;

import org.dstorerdb.encryptor.exceptions.SmallKeyException;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 *
 * @author martin
 */
public class Key {
    private final byte[] byteKey;

    public Key(String strKey) {
        if (strKey.length() < 2) {
            throw new SmallKeyException("La llave del encriptador es muy pequeña.\n"
                    + "Debe ser mínimo de 2 caracteres");
        }
        this.byteKey = strKey.getBytes(StandardCharsets.UTF_8);
    }

    public char getChar(int pos){
        return (char) byteKey[pos];
    }
    
    public char getFirstChar(){
        return (char) byteKey[0];
    }
    
    public int getKeyLenght(){
        return byteKey.length;
    }
    
//    public String getChars(int origin, int end){
//        StringBuilder sBuilder = new StringBuilder();
//        for (int i = origin; i < end; i++)
//            sBuilder.append((char)byteKey[i]);
//        return sBuilder.toString();
//    }

    public char[] getChars(int origin, int end){
        byte[] copy = Arrays.copyOfRange(byteKey, origin, end);
        return new String(copy).toCharArray();
    }
    
    public char[] getChars(){
        return toString().toCharArray();
    }
    
    
    public byte[] getByteKey() {
        return byteKey;
    }

    @Override
    public String toString() {
        return new String(byteKey);
    }
    
}
