/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.encryptor.test;

import org.dstorerdb.encryptor.encryptor.Encryptor;
import org.dstorerdb.encryptor.exceptions.InvalidTextException;

import java.util.Arrays;

/**f
 *
 * @author martin
 */
public class Main {
    public static void main(String[] args) throws InvalidTextException {
        String str = "12345";
        Encryptor encryptor = new Encryptor("xd");
        String enc = encryptor.encrypt(str);
        String desec = encryptor.decrypt(enc);
        
        System.out.println("Text: "+Arrays.toString(str.getBytes()));
        System.out.println("Encriptado: "+Arrays.toString(enc.getBytes()));
        System.out.println("Desencriptado: "+desec);
        
    }
    
    // Tiempo en microsegundos
    public static long currentTime(){
        return System.nanoTime()/1000;
    }
    
}
